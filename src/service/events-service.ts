import { Events } from '../entity/events';
import {model} from "../model/model";

export class EventsService {

    public createEvent(event: Events) {
        const events = model(new Events())
        const eventsModel = new events(event);

        return eventsModel.save();
    }

    public findEvent(eventName: string) {
        const events = model(new Events());
        return events.find({event: new RegExp(`${eventName}`, 'i') });
    }
}