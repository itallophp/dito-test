import {IPayloadEvents} from "../entity/payload-events";
import {ITimelineCustomData} from "../entity/timeline-custom-data";
import {ITimelineEvents} from "../entity/timeline-events";
import {EventsEnum} from "../enums/events-enum";
import {ITimeline} from "../entity/timeline";
import {IProduct} from "../entity/product";

export class TimelineService {

    private payloadEvents: IPayloadEvents;

    constructor(payloadEvent: IPayloadEvents) {
        this.payloadEvents = payloadEvent;
    }

    private getValue = (customData: ITimelineCustomData[], keyValue: string) => {
        const data = customData.find((data: ITimelineCustomData) => data.key === keyValue)
        return data === null ? null : data.value;
    }

    private getTransationId = (customData: ITimelineCustomData[]) => {
        return this.getValue(customData, "transaction_id")
    }

    private getStoreName = (customData: ITimelineCustomData[]) => {
        return this.getValue(customData, "store_name");
    }

    private getEventsByType = (type: string) => {
        return this.payloadEvents.events.filter((event: ITimelineEvents) => {
            return event.event === type;
        })
    }

    private getEventsByTransactions = (transactionId) => {
        return this.getEventsByType(EventsEnum.PRODUCT_BUYED)
            .filter((event: ITimelineEvents) => this.getTransationId(event.custom_data).toString() === transactionId.toString())
    }

    public handleTimeline()
    {
        return this.getEventsByType(EventsEnum.BUYED)
            .map((eventBuy: ITimelineEvents) => {
                const transactionId = this.getTransationId(eventBuy.custom_data)
                const eventsProductBuy = this.getEventsByTransactions(transactionId)
                const products = eventsProductBuy.reduce((products: IProduct[], event: ITimelineEvents) => {
                    const price = parseFloat(this.getValue(event.custom_data, "product_price"))
                    const name  = this.getValue(event.custom_data, "product_name")

                    products.push({
                        name,
                        price
                    })

                    return products
                }, [])

                return {
                    timestamp: eventBuy.timestamp,
                    revenue: eventBuy.revenue,
                    transaction_id: transactionId,
                    store_name: this.getStoreName(eventBuy.custom_data),
                    products
                }
            })
    }
}