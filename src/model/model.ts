import {Typegoose} from "typegoose";
import {connection} from "../database/connection"

export const model = (entity: Typegoose) =>
    entity.getModelForClass(entity, {
        existingConnection: connection
    })