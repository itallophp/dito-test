export interface ITimelineCustomData {
    key: string,
    value: string
}