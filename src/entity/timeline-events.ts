import {ITimelineCustomData} from "./timeline-custom-data";

export interface ITimelineEvents {
    event: string,
    timestamp: Date,
    revenue?: number,
    custom_data: ITimelineCustomData[]
}