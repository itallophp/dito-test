import {IProduct} from "./product";

export interface ITimeline {
    timestamp: Date,
    revenue: number,
    transaction: string,
    store_name: string,
    products: IProduct[]
}