import {prop, Typegoose} from "typegoose";


export class Events extends Typegoose {
    @prop({required: true})
    event: string;

    @prop({default: Date.now()})
    timestamp?: Date
}