import {ITimelineEvents} from "./timeline-events";


export interface IPayloadEvents {
    events: ITimelineEvents[]
}