import * as mongoose from "mongoose";

const host = process.env.MONGO_HOST || "localhost";
const port = process.env.MONGO_PORT || 27017;
const database = process.env.MONGO_COLLECT || "test1";

export const connection = mongoose.createConnection(`mongodb://${host}:${port}/${database}`);