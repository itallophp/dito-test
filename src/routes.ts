import { Router, Request, Response, NextFunction } from "express"
import * as timeout from "connect-timeout"
import { InstanceType } from 'typegoose';
import { Events } from './entity/events';
import { IPayloadEvents } from "./entity/payload-events";
import { TimelineService } from "./service/timeline-service";
import * as fetch from 'node-fetch';
import {EventsService} from "./service/events-service";

const router = Router()

router.get('/transaction', timeout('60s'), async (req: Request, res: Response) => {
    const resp = await fetch("https://storage.googleapis.com/dito-questions/events.json")
    const resposta = await resp.json<IPayloadEvents>();
    const timelineService = new TimelineService(resposta);
    const timelines = timelineService.handleTimeline();

    timelines.sort((timelineA, timelineB) => {
        const timestampA = new Date(timelineA.timestamp);
        const timestampB = new Date(timelineB.timestamp);

        if (timestampA === timestampB) {
            return 0;
        }

        return timestampA < timestampB ? 1 : -1;
    })

    res.status(200).send({timeline: timelines});
});

router.post('/events', timeout('10s'), async (req: Request, res: Response) => {


    try {
        const eventService = new EventsService();
        const result = await eventService.createEvent(req.body);

        res.header({'id': result._id})
        res.status(204).send();
    } catch (e) {
        res.status(400).send({error: e.message});
    }
});

router.get('/events', timeout('20s'), async (req: Request, res: Response) => {
    try {
        const param = req.query.name || "";
        const eventService = new EventsService();
        const results = await eventService.findEvent(param);

        const r = results.map((result: InstanceType<Events>) => {
            return {
                event: result.event,
                timestamp: result.timestamp
            }
        })

        res.status(200).send(r);
    } catch (e) {
        res.status(400).send({error: e.message});
    }
})


//default timeout. place your routes before this
router.use(timeout('5s'))

export default router